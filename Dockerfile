# FROM node:latest

# WORKDIR /usr/src/app

# ARG NODE_ENV
# ENV NODE_ENV $NODE_ENV
# COPY package.json /usr/src/app/
# RUN npm install
# COPY . /usr/src/app

# #CMD [ "npm", "run", "start-server" ]
# #CICD
# # replace this with your application's default port
# EXPOSE 8080



FROM node:9.3.0-alpine

# install static server
RUN npm install -g serve

# create a 'tmp' folder for the build and make it the current working directory
WORKDIR /app/tmp

# copy only the package.json to take advantage of cached Docker layers
COPY package.json .

# install project dependencies
RUN npm install

# copy project files and folders to the working directory
COPY . .

# build for production with minification
RUN npm run build

# make the 'app' folder the current working directory
WORKDIR /app

# clean up (i.e. extract 'dist' folder and remove everything else)
RUN mv tmp/dist dist && rm -fr tmp

EXPOSE 8080
# CMD [ "serve", "--single", "--port", "8080", "dist" ]